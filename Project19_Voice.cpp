﻿
#include <iostream>
#include <string>
#include <array>
#include <iomanip>
#include <stdint.h>

class Animal {
public:
    Animal() {}
    virtual void voice() {
        std::cout << "voice";
    }
};

class Dog : public Animal {
public:
    void voice() override {

        std::cout << "Woof" << "\n";
    }
};

class Cat : public Animal {

    void voice() override {
        std::cout << "Miau" << "\n";
    }
};

class Cow : public Animal {

    void voice()override {
        std::cout << "Muuu" << "\n";
    }
};

int main()
{
    Dog* drudjok = new Dog;
    Cat* barsik = new Cat;
    Cow* zorka = new Cow;
    Animal* animals[3] = { drudjok, barsik, zorka };
    for (int i = 0; i < 3; i++)
    {
        animals[i]->voice();

    }






}

